import React from 'react'

const Finish=(props)=> {
 // console.log("finish", props)
  return (
    <div className='round'>
   <button className="ended" onClick={()=>props.onSelect(props.todo)}>--</button>
    </div>
  )
}
export default Finish;