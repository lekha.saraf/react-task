
import { useState } from 'react';
import TodoCard from '../todoCard'
import Footer from '../footer'
import Sidediv from '../sidediv'
const Todo = ()=>{
  //  const [input, setInput] = useState('');
  const [todos, setTask] = useState([]);
  const addTodo = text => {
    setTask(todos=> [...todos, {task:text, isDone:false}])
  }
  
  const Del = (id)=>{ setTask((todos)=>{return todos.filter(
    (list, index)=>{return index!==id})})
  }
  const clearComplete = ()=>{
      let newTodo =todos.filter(lists=>lists.isDone===false) 
      console.log("check new todo", newTodo)
     setTask(newTodo); 
  }
  const ended = (id)=>{
    let newTodo =[...todos];
    newTodo[id].isDone===false?newTodo[id].isDone=true:newTodo[id].isDone=false;
     setTask(newTodo); 
  } 

  const complete = todos.filter(lists=>lists.isDone===true)  
  const active = todos.filter(lists=>lists.isDone===false)  

    return(<>
        <h1> todos </h1>
<div className='textInput'> 
<i class="fa-solid fa-angle-down"></i> 
{/* onChange */}
<input type="text" placeholder="what needs to be done?" autoFocus onKeyPress={e => {
 if (e.key === "Enter") {
   addTodo(e.target.value)
   e.target.value=''
 } 
}} />  </div>
<TodoCard todo={todos} onSelect={Del} complete={complete} ended={ended} active={active} clear={clearComplete}/>
    <Footer/>
    <Sidediv/>
</>
    )
}
export default Todo;