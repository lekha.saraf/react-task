//import React from 'react'

const Sidediv = ()=> {
  return (
   <div className="side">
        <h3>Backbone.js</h3>
        <br/>
        <header >
        <span>
            <h5>Example</h5>
            <a href="https://github.com/tastejs/todomvc/tree/gh-pages/examples/backbone">Source</a>
            <br/>
            <br/>
            <h5>Require.js &#38; Backbone.js</h5>
            <a href="https://todomvc.com/examples/backbone_require/">Demo</a>,
            <a href="https://todomvc.com/examples/backbone_require/">Source</a>
            <br/>
            <br/>
            <h5>Enyo &#38; Backbone.js</h5>
            <a href="https://todomvc.com/examples/backbone_require/">Demo</a>,
            <a href="https://todomvc.com/examples/backbone_require/">Source</a>
            <br/>
            <br/>
            <h5>TypeScript &#38; Backbone.js</h5>
            <a href="https://todomvc.com/examples/backbone_require/">Demo</a>,
            <a href="https://todomvc.com/examples/backbone_require/">Source</a>
            <br/>
            <br/>
        </span>
    </header>
    <hr/>
    <blockquote>
        <p>Backbone.js gives structure to web applications by providing models with key-value binding and custom events, collections with a rich API of enumerable functions, views with declarative event handling, and connects it all to your existing API over a RESTful JSON interface.</p>
    </blockquote>
    <footer>
    <a href="http://backbonejs.org">Backbone.js</a>
    </footer>
    <hr/>
    <h4>Official Resources</h4>
    <ul >
        <li >Annotated source code</li>
        <li >Applications built with Backbone.js</li>
        <li >FAQ</li>
    </ul>
    <h4>Articles and Guides</h4>
    <ul >
        <li >Developing Backbone.js Applications</li>
        <li >Collection of tutorials, blog posts, and example sites</li>
    </ul>
    <h4>Community</h4>
    <ul >
        <li >Backbone.js on Stack Overflow</li>
        <li >Google Groups mailing list</li>
        <li >Backbone.js on Twitter</li>
    </ul>
    <hr/>
    <br/>
    <em>
    If you have other helpful links to share, or find any of the links above no longer work, please 
    <a href={"http://backbonejs.org"}> let us know.</a>
    </em>
    </div>
  )
}
export default Sidediv;
