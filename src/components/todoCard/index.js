import React, { useState } from 'react'
import Remove from '../remove'
import Finish from '../finish'

const TodoCard=(props)=> {
  const styling = (d) => {
    let text = [];
    if (d.isDone === true) {
      text.push('myClass')
    } else {
      text.push('otherClass')
    }
    console.log(text)
    return text
  }

  const [select, setSelect] = useState("all")
  const items = () => {
    if (select === "all") return props.todo.map((d, index) => <li className={styling(d)} key={d.index}>
      <Finish todo={index} onSelect={() => props.ended(index)} />{d.task}
      <Remove id={index} onSelect={() => props.onSelect(index)} />
    </li>);
    else if (select === "completed") return props.complete.map((d, index) => <li className={styling(d)} key={d.index}><Finish todo={index} onSelect={() => props.ended(index)} />{d.task}
      <Remove id={index} onSelect={() => props.onSelect(index)} />
    </li>);
    else if (select === "active") return props.active.map((d, index) => <li key={d.index}><Finish todo={index} onSelect={() => props.ended(index)} />{d.task}
      <Remove id={index} onSelect={() => props.onSelect(index)} />
    </li>);

  }
  return (

    <ul>{items()}
      {props.todo.length > 0 ?
        <li className='bottom'>
          <span className='left'>{props.active.length} Item left </span>
          <div className='together'>
            <button onClick={() => setSelect('all')} className='All'>All</button>
            <button onClick={() => setSelect("active")} className='Active'>Active</button>
            <button onClick={() => setSelect("completed")} className='Completed'>Completed</button>
          </div>
          <button className='clear' onClick={() => props.clear()} >Clear Complete</button></li> : ''}  </ul>
  )
}

export default TodoCard;
