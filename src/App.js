import Todo from './components/todo'
import './App.css';

const App=() => {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}

export default App;
